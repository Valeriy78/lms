import datetime
import random

from django.db import models
from faker import Faker


class Group(models.Model):
    group_number = models.SmallIntegerField()
    subject = models.CharField(max_length=128)
    location = models.CharField(max_length=128)
    first_lesson_date = models.DateField(null=True)
    number_of_lessons = models.SmallIntegerField(default=0, null=True)
    number_of_homeworks = models.SmallIntegerField(default=0, null=True)

    def __str__(self):
        return f"Group {self.group_number} {self.location} {self.subject}"

    def __repr__(self):
        return f"Group {self.group_number} {self.location} {self.subject}"

    @classmethod
    def generate_instances(cls, count):
        for _ in range(count):
            faker = Faker()
            cls.objects.create(
                group_number=random.randint(100000, 999999),
                subject=random.choice(['Python', 'PHP', 'Java', 'C#', 'Front-end', 'DevOps']),
                location=random.choice(['Kyiv', 'Odesa', 'Dnipro', 'Kharkiv']),
                first_lesson_date=faker.date_time_between(
                    start_date=datetime.date(2022, 1, 13),
                    end_date=datetime.date(2022, 2, 23)
                ).date(),
                number_of_lessons=random.randint(30, 50),
                number_of_homeworks=random.randint(20, 50)
            )
