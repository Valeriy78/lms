from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from webargs.djangoparser import use_args

from students.models import Student
from students.utils import format_records


def hello(request):
    return HttpResponse("Hello, World!")


@use_args(
    {
        'first_name': fields.Str(
            required=False
        ),
        'search_text': fields.Str(
            required=False
        )
    },
    location='query'
)
def get_students(request, parameters):
    form = """
            <form>
              <label>First name:</label><br>
              <input type="text" name="first_name"><br>

              <label>Search :</label><br>
              <input type="text" name="search_text" placeholder="Enter text to search"><br><br>

              <input type="submit" value="Submit"/>
            </form>
        """

    students = Student.objects.all()
    search_fields = ['first_name', 'last_name', 'email']
    for param_name, param_value in parameters.items():
        if param_value:
            if param_name == 'search_text':
                or_filter = Q()
                for field in search_fields:
                    or_filter = or_filter | Q(**{f"{field}__icontains": param_value})
                students = students.filter(or_filter)
            else:
                students = students.filter(**{param_name: param_value})
    result = format_records(students)
    response = form + result

    return HttpResponse(response)


@csrf_exempt
def create_student(request):
    form = """
                <form method="POST">
                  <label>First name:</label><br>
                  <input type="text" name="first_name"><br>

                  <label>Last name:</label><br>
                  <input type="text" name="last_name"><br>

                  <label>Email:</label><br>
                  <input type="email" name="email"><br><br>

                  <input type="submit" value="Submit"/>
                </form>
            """

    if request.method == "POST":
        Student.objects.create(
            **{key: value for key, value in request.POST.items()}
        )
        return HttpResponseRedirect('/students')

    return HttpResponse(form)
