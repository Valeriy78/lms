from django.urls import path
from students.views import hello, get_students, create_student

urlpatterns = [
    path('hello/', hello),
    path('', get_students),
    path('create/', create_student)
]
