import random

from django.db import models
from faker import Faker


class Teacher(models.Model):
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    email = models.EmailField(max_length=128)
    birthdate = models.DateField(null=True)
    group_number = models.SmallIntegerField(null=True)
    homeworks_checked = models.SmallIntegerField(default=0, null=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.id}"

    def __repr__(self):
        return f"{self.first_name} {self.last_name} {self.id}"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker('Uk')
        for _ in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birthdate=faker.date_time_between(
                    start_date="-120y", end_date="-21y"
                ),
                group_number=random.randint(100000, 999999),
                homeworks_checked=random.randint(0, 100)
            )
